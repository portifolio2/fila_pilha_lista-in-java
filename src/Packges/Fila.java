/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packges;

import java.io.IOException;

/**
 *
 * @author Kayc Kennedy
 */
public class Fila {
    
     /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private NoFila Fila[];
    Lista minhaLista = new Lista();
    PilhaContigua minhaPilhaC = new PilhaContigua();
    
    private
        int inicioDaFila;
        int finalDaFila;
        int quantidadeDeNos;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
        
    public Fila() { }
        
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    
    // Método para acessar a fila.
    public NoFila[] getFila() {
        return this.Fila;
    }
    
    // Método para inicializar (registrar) a fila.
    public void setFila(NoFila[] _fila) {
        this.Fila = _fila;
    }

    // Método para resgatar o início da fila.
    public int getInicioDaFila() {
        return this.inicioDaFila;
    }

    // Método para alterar o início da fila.
    public void setInicioDaFila(int _inicioDaFila) {
        this.inicioDaFila = _inicioDaFila;
    }

    // Método para resgatar o final da fila
    public int getFinalDaFila() {
        return this.finalDaFila;
    }

    // Método para alterar o final da fila.
    public void setFinalDaFila(int _finalDaFila) {
        this.finalDaFila = _finalDaFila;
    }

    // Método para resgatar a quantidade de nós.
    public int getQuantidadeDeNos() {
        return this.quantidadeDeNos;
    }

    // Método para alterar a quantidade de nós.s
    public void setQuantidadeDeNos(int _quantidadeDeNos) {
        this.quantidadeDeNos = _quantidadeDeNos;
    }
    
   /*-----------------------------------
           OPERAÇÕES DO TAD FILA C
    -----------------------------------*/
    
    // Método para criar a fila
    // _qDN = quantidade de nós
    public void queue(int _qDN) throws IOException{
        logTxt.gravarArquivo("Preenchimento da fila com " + _qDN + " posições");

        this.setFila(new NoFila[_qDN]);
        
        this.setInicioDaFila(0);
        this.setFinalDaFila(-1);
        this.setQuantidadeDeNos(0); 
        
    }
    
    // Método para verificar se a fila está vazia.
    private boolean isEmpty(){
        
        // Quando o final da fila passa a ser menor "em valor"
        // do que o início da fila, então a fila está vazia.
        return ( this.getFinalDaFila() < this.getInicioDaFila() );
        
    }
    
    // Método para verificar se a fila está cheia.
    public boolean isFull(){
        
        // Caso existe algum elemento na última posição do vetor,
        // dizemos que a Fila está cheia.
        return ( this.getFinalDaFila() == (this.getFila().length-1) );
        
    }
       
    // Método para adicionar dados.
    public boolean enqueue(int _c, String _n, String _e) throws IOException{
        
        // Se a fila não estiver cheia,
        if (!this.isFull()){
            
            // Diz qual a posição válida para inserir um nó.
            this.setFinalDaFila(this.getFinalDaFila()+1);
            
            // Cria um novo nó, insere os dados do usuário e
            // insere o nó na posição 'finalDaFila'.
            this.getFila()[this.getFinalDaFila()] = new NoFila(_c,_n, _e);

            // Atualiza a quantidade de nós da fila.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()+1);
            
            logTxt.gravarArquivo("Pessoa adiconada a Fila: Nome: " + _n + ", Telefone de contato: " + _c + ", Email: " + _e);

            return true;
        }
        logTxt.gravarArquivo("Erro ao adicionar pessoa a Fila ");
        return false;
    }
    
    // Método para desenfileirar dados.
    // O nó a ser removido sempre será o que está na 1ª posição da Fila.
    public boolean dequeue() throws IOException{
        
        // Se foi possível apresentar os dados do nó
        // que está na 1ª posição da Fila, efetiva a remoção.
        if (this.front()){
        
            // Percorre a fila, do início para o final,
            // deslocando os elementos da direita para esquerda,
            // para evitar buraco na Fila.
            for (int i = this.getInicioDaFila(); i < this.getFinalDaFila(); i++)
                this.getFila()[i] = this.getFila()[i+1];
            
            // Atualiza o valor do final da fila.
            this.setFinalDaFila(this.getFinalDaFila()-1);

            // Atualiza a quantidade de nós da fila.
            this.setQuantidadeDeNos(this.getQuantidadeDeNos()-1);
            
            logTxt.gravarArquivo("Primeiro da Fila removido");
            
            return true;
        }
        
        logTxt.gravarArquivo("Erro ao Remover Primeiro da Fila");
        
        return false;
    }
   
    // Método que apresenta os dados do objeto que está na 1ª posição da Fila.
    public boolean front() throws IOException{
        
        // Se a fila não estiver vazia, apresenta os dados.
        if (!this.isEmpty()) {
            
            System.out.print("Número: ");
            System.out.print(this.getFila()[this.getInicioDaFila()].getCodigoUsuario());
        
            System.out.print("\nNome: ");
            System.out.print(this.getFila()[this.getInicioDaFila()].getNomeUsuario());
            
            System.out.print("\nEmail: ");
            System.out.print(this.getFila()[this.getInicioDaFila()].getEmailUsuario());
            
            logTxt.gravarArquivo("Buscando o Primeiro Usuário da Fila, Usuario: " + this.getFila()[this.getInicioDaFila()].getNomeUsuario());
            return true;
            
        }
         logTxt.gravarArquivo("Erro ao Acessar Primeira Posição da Fila");
        return false;
    }
    
    // Método que retorna todos os dados da Fila em formato de "String".
    public String print() throws IOException{
        
        String informacoes = "";

        // Se a fila não estiver vazia, percorre a fila. 
        if (!this.isEmpty())
            
            // Percorrendo a fila para montar a String.
            for (int i = this.getInicioDaFila(); i <= this.getFinalDaFila(); i++)
                informacoes += this.getFila()[i].toStringDadosUsuario();
        
        return informacoes;
        
    }

    // Método que retorna a quantidade de nós presentes na fila.
    public int size() throws IOException{
        
        logTxt.gravarArquivo("Quantidade de Pessoas da Fila Visualizada");
        return this.getQuantidadeDeNos(); 
        
    }
    
    // Método para limpar / esvaziar a fila.
    public void clear() throws IOException{
        
        // Se a fila não estiver vazia,
        // inicia os valores dos atributos da classe.
        if (!this.isEmpty()){
            
            this.setInicioDaFila(0);
            this.setFinalDaFila(-1);
            this.setQuantidadeDeNos(0);    
            logTxt.gravarArquivo("Fila Esvaziada com Sucesso");

        }
        
        
    }
    
}
