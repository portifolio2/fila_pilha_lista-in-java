/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packges;

/**
 *
 * @author Kayc Kennedy
 */

public class NoFila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private int codigoUsuario;
    private String nomeUsuario;
    private String emailUsuario;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    public NoFila(){}
    // _codU  = código do usuário
    // _nomeU = nome do usuário
    public NoFila(int _codU, String _nomeU, String _email) {
        this.codigoUsuario = _codU;
        this.nomeUsuario   = _nomeU;
        this.emailUsuario = _email;
    }
    
    
    /*-----------------------------------
                MÉTODOS DA CLASSE
    -----------------------------------*/
    
    // Método para resgatar o código do usuário
    public int getCodigoUsuario() {
        return this.codigoUsuario;
    }

    // Método para registrar o código do usuário
    // _codU  = código do usuário
    public void setCodigoUsuario(int _codU) {
        this.codigoUsuario = _codU;
    }

    // Método para resgatar o nome do usuário
    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    // Método para registrar o nome do usuário
    // _nomeU = nome do usuário
    public void setNomeUsuario(String _nomeU) {
        this.nomeUsuario = _nomeU;
    }
    
    // Método para resgatar o email do usuário
    public String getEmailUsuario(){
        return this.emailUsuario;
    }
    
    // Método para registrar o email do usuário
    // _email = nome do usuário
    public void setEmailUsuario(String _email){
        this.emailUsuario = _email;
    }

    /*-----------------------------------
            OUTROS MÉTODOS DA CLASSE
    -----------------------------------*/

    public String toStringDadosUsuario(){

        String informacoes = "";

        informacoes = "numero: " + this.getCodigoUsuario() + "\n";
        informacoes += "Nome:   " + this.getNomeUsuario()   + "\n";
        informacoes += "email:  " + this.getEmailUsuario() + "\n\n";

        return informacoes;

    }
    
}
