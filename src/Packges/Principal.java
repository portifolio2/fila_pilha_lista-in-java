/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packges;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Kayc Kennedy
 */
public class Principal {

    public static void main(String[] args) throws IOException {
                
        int opcao = -1; // Opção para trabalhar o menu principal
        
        int numeroTelefone;  // Numero de contato do usuário
        String nome;    // Nome de usuário
        String email; // Email do usuário
        //int ingressos = 15; // Quantidade de ingressos
        
        
        // Criando um objeto para trabalhar dados pelo teclado.
        Scanner entrada = new Scanner(System.in);
        
        // Criando uma instância da FilaContigua.
        Fila minhaFilaC = new Fila();
        
        // Criando uma instancia da PilhaContigua
        PilhaContigua listaIngressos = new PilhaContigua();
        
        // Criando uma instancia de Lista 
        Lista listaUsuario = new Lista();
        
        // Posição que será inserida na lista
        int posicao = listaUsuario.getFimDaLista();
        
        // Criando instancia do NoFila
        NoFila dadosUsuarioFila = new NoFila();
               
        //Tamanho da Pilha
        int tamanhoPilha = 75;
        
        logTxt.gravarArquivo("");
        logTxt.gravarArquivo("-----------------------------------------------------------");        
        logTxt.gravarArquivo("---------------INICIO DA EXECUÇÃO DO SISTEMA---------------");
        
        // Enquanto a opção for diferente de zero.
        while (opcao != 0){
            
            System.out.println("============================");
            System.out.println(" 1 - Criar Fila ");
            System.out.println(" 2 - Adicionar Pessoas a Fila ");
            System.out.println(" 3 - Remover Primeiro da Fila ");
            System.out.println(" 4 - Mostrar Primeiro da Fila");
            System.out.println(" 5 - Percorrer a Fila");
            System.out.println(" 6 - Quantidade de Pessoas na Fila");
            System.out.println(" 7 - Esvaziar a Fila");
            System.out.println(" 8 - Mostrar Valores Da Pilha          ");
            System.out.println(" 9 - Atribuir ingresso ao usuario          ");

            System.out.println("============================");
            System.out.println(" 0 - Sair da aplicação      ");
            System.out.println("============================");
            
            System.out.print("Opção = ");
            opcao = entrada.nextInt();
            
            System.out.print("\n");
           

            switch (opcao){
            
                case 0:	System.out.println("---------------------");
                        System.out.println(" SAINDO DA APLICAÇÃO ");
                        System.out.println("---------------------");
                        
                        logTxt.gravarArquivo("--------------- FIM DA EXECUÇÃO DO SISTEMA ---------------");
                        logTxt.gravarArquivo("-----------------------------------------------------------");        
                        
                        //Fila Apagada
                        minhaFilaC.clear();

                        entrada.close();

                        break;
                
                case 1: 
                        
                        int cpEvento = 80;
                        
                        //Pilha Criada
                        listaIngressos.criarPilha(tamanhoPilha);
                        logTxt.gravarArquivo("Pilha criada com o tamanho: " + tamanhoPilha);
                        
                        //Lista Criada de Acordo com o Tamanho da Pilha
                        listaUsuario.criarLista(tamanhoPilha);
                        logTxt.gravarArquivo("Lista de usuarios criada com o tamanho: " + tamanhoPilha);
                        
                        //Pilha Preenchida com valores aleatorios
                        listaIngressos.preenche();
                        
                        System.out.print("O evento no momento tem " + cpEvento + " pessoas \n");
                        
                        //Fila Criada 
                        minhaFilaC.queue(cpEvento);
                        System.out.println("Fila criada com sucesso -> Fila["+cpEvento+"]");
                        break;

                case 2:	System.out.println("-------------------");
                        System.out.println("    Adicionando   ");
                        System.out.println("-------------------");

                        do{
                            System.out.print("Informe o numero para contato: ");
                            numeroTelefone = entrada.nextInt();
                        }while(numeroTelefone < 0); // Enquanto digitar um código negativo, repete.
                        
                        //Colhimento dos Dados do Usuario
                        System.out.print("Informe o nome: ");
                        nome = entrada.next();
                        
                        System.out.print("Informe o email:");
                        email = entrada.next();
                        
                        //Adicionando o Usuario a Fila
                        if(minhaFilaC.enqueue(numeroTelefone, nome, email)){
                            System.out.println("\nDados do usuário inserido com sucesso!");
                        }
                        else{
                            System.out.println("Aconteceu algo...");
                        }
                        
                        break;
                
                case 3:	System.out.println("-------------------");
                        System.out.println("  DESENFILEIRANDO  ");
                        System.out.println("-------------------");
                        
                        //Removendo os Dados de usuario da primeira posição da fila
                        if (minhaFilaC.dequeue())
                            System.out.println("\nDados do usuário excluídos com sucesso!");
                        else
                            System.out.println("Fila vazia...");
                        
                        break;
                
                case 4: System.out.println("-------------------");
                        System.out.println("   FRENTE DA FILA  ");
                        System.out.println("-------------------");
                        
                        //Buscando os Dados do Primeiro da Fila
                        if(!minhaFilaC.front())
                            System.out.println("Fila vazia...");
                            
                        break;
                
                case 5: System.out.println("-------------------");
                        System.out.println("        FILA       ");
                        System.out.println("-------------------");
                        
                        //Buscando a Fila completa
                        String resultado = minhaFilaC.print();

                        if ( !"".equals(resultado) ){
                            System.out.println(resultado);
                            logTxt.gravarArquivo("Fila Completa de Pessoas Acessada");
                        }
                        else{
                            System.out.println("Pilha vazia...");
                            logTxt.gravarArquivo("Erro ao Acessar Fila Completa de Pessoas");
                        }
                        break;
                
               
                    
                case 6: System.out.println("-------------------");
                        System.out.println("  TOTAL DE PESSOAS ");
                        System.out.println("-------------------");
                        
                        //Buscando quantidade de pessoas na fila
                        System.out.println(minhaFilaC.size());
                        
                        break;

                case 7: System.out.println("-------------------");
                        System.out.println("     ESVAZIANDO    ");
                        System.out.println("-------------------");
                        
                        //Limpando a Fila
                        minhaFilaC.clear();
                        logTxt.gravarArquivo("Fila Esvaziada");

                        System.out.println("Fila vazia...");
                        
                        break;
                case 8: System.out.println("-------------------");
                        System.out.println("       Pilha       ");
                        System.out.println("-------------------");
                        
                        //Buscando Valores da Pilha
                        listaIngressos.print();
                        break;
                case 9:                  
                    //Através dos metodos Get, busco os dados de usuario e de ingressos, presentes em minhas estruturas de dados
                    String codigoIngresso = listaIngressos.top();//Codigo Ingresso
                    String nomeUsuario = minhaFilaC.getFila()[minhaFilaC.getInicioDaFila()].getNomeUsuario();//Nome Usuario
                    int numero = minhaFilaC.getFila()[minhaFilaC.getInicioDaFila()].getCodigoUsuario(); //Numero Telefone
                    String emailUsuario = minhaFilaC.getFila()[minhaFilaC.getInicioDaFila()].getEmailUsuario(); // Email          
                    
                    //Insiro os dados na Lista
                    if(listaUsuario.inserirDados(posicao, codigoIngresso, nomeUsuario, numero, emailUsuario)){
                    
                        System.out.println("---------- Dados Adicionados a Lista ----------");
                        // Remove o primeiro da fila
                        minhaFilaC.dequeue();
                    
                        // Remove o primeiro ingresso da pilha
                        listaIngressos.pop();
                        }else{
                            System.out.println("Aconteceu Algum erro...");
                            }
                    
                        break;

                        
                default: System.out.println("Entrada inválida!");              
            }
            System.out.print("\n");
        }   
    }
}
