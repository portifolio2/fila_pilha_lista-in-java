package Packges;

import java.io.*;
/**
 *
 * @author Kayc Kennedy
 */

public final class logTxt {
    
    public static void gravarArquivo(String texto) throws IOException{
        //Caminho do arquivo 
        File arquivo = new File("src/Packges/logApp.txt");
       
        try {
            //Se o arquivo não existe crie um novo
            if (!arquivo.exists()) {
            //cria um arquivo (vazio)
            arquivo.createNewFile();
            }
  
            //escreve no arquivo
            FileWriter fw = new FileWriter(arquivo, true);
            BufferedWriter bw = new BufferedWriter(fw);
           
            bw.write(texto);
            bw.newLine();
           
            //Encerro as conexões 
            bw.close();
            fw.close();
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    
    }
}

    

