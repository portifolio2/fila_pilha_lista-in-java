package Packges;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Kayc Kennedy
 */
public class PilhaContigua {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    int numeroNos;
    int sala01[] = new int[14]; int qtdSala01 = 0;
    int sala02[] = new int[14]; int qtdSala02 = 0;
    int sala03[] = new int[14]; int qtdSala03 = 0;
    int sala04[] = new int[14]; int qtdSala04 = 0;
    int sala05[] = new int[14]; int qtdSala05 = 0;
    
    String codStringSala[] = {"S01", "S02", "S03", "S04", "S05"}; 
    
    
    Random numeroRandom = new Random();
    
    private
        NoPilha Pilha[];
        int topo;

    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    
    public PilhaContigua(){ }
    
    /*-----------------------------------
         MÉTODOS get E set DA CLASSE
    -----------------------------------*/
    
    // Método para acessar a Pilha.
    public NoPilha[] getPilha() {
        return this.Pilha;
    }
    
    // Método para inicializar (registrar) a pilha.
    public void setPilha(NoPilha[] _p) {
        this.Pilha = _p;
    }
    
    // Método para acessar o topo da pilha.
    public int getTopo() {
        return this.topo;
    }
    
    // Método para alterar (registrar) o topo da pilha.
    public void setTopo(int _t) {
        this.topo = _t;
    }
    
    /*-----------------------------------
           OPERAÇÕES DO TAD PILHA C
    -----------------------------------*/
    
    // Método para criar a pilha
    // _qdn = quantidade de nós
    public void criarPilha(int _qdn){
        this.setTopo(-1);
        this.setPilha(new NoPilha[_qdn]);
        this.numeroNos = _qdn;
        
    }
    
    // Método para verificar se a pilha está cheia.
    public boolean isFull(){

        return this.getTopo() == (this.getPilha().length-1);
    }
  
    // Método para verificar se a pilha está vazia.
    public boolean isEmpty(){
        return this.getTopo() == -1;
    }
    
    //Metodo para Preencher a Pilha com Valores Aleatorios 
    public void preenche() throws IOException{
        int ingressoAleatorio;
        logTxt.gravarArquivo("Preenchimento da pilha com " + numeroNos + " posições");
        
        //Laço de repetição para buscar um valor aleatorio de ingresso e concatecar com um codigo de sala também aleatorio
        //O laço de repetição irá executar a quantidade de numero de nós da pilha
        for(int i = 0; i < numeroNos; i++){
            ingressoAleatorio = numeroRandom.nextInt(99);
            String codigoSala = codStringSala[numeroRandom.nextInt(4)];
            
            if(qtdSala01 < 15 && codigoSala == "S01"){
                push( codigoSala  + ingressoAleatorio);    
                qtdSala01++;
            }
            else if (qtdSala02 < 15 && codigoSala == "S02"){
                push( codigoSala  + ingressoAleatorio);    
                qtdSala02++;
            }
            else if (qtdSala03 < 15 && codigoSala == "S03"){
                push( codigoSala  + ingressoAleatorio);    
                qtdSala03++;
            }
            else if (qtdSala04 < 15 && codigoSala == "S04"){
                push( codigoSala  + ingressoAleatorio);    
                qtdSala04++;
            }
            else if (qtdSala05 < 15 && codigoSala == "S05"){
                push( codigoSala  + ingressoAleatorio);    
                qtdSala05++;              
                                
            }
        }
    }
    // Método para empilhar dados na pilha.
    public void push(String _c){
       
        // Se a pilha não estiver cheia,
        if (!this.isFull()){

            this.setTopo(this.getTopo() + 1); // Atualiza o topo

            // Cria um novo nó, insere os dados do usuário.
            // Insere o nó criado na posição topo.
            this.getPilha()[this.getTopo()] = new NoPilha(_c);
        }      
    }
    
    // Método para desempilhar dados da pilha.
    public boolean pop(){
        
        // Verifica se a pilha não está vazia de forma indireta.
        if (this.top() != "Codigo Invalido"){
            this.setTopo(this.getTopo() - 1); // Atualiza o topo.
            return true;
        }

        return false;
    }
    
    // Método para apresentar os dados de quem está no topo
    public String top(){
        
        // Se a pilha não estiver vazia, apresenta os dados que estão no topo.
        if (!this.isEmpty()){
            
            return ("Codigo Sala: " + this.getPilha()[this.getTopo()].getCodigoSala());
        }

        return "Codigo Invalido";
    }
    
    // Mostrando a quantidade de nós da pilha.
    public int size(){
        // Retorna sempre o valor de 'topo' + 1,
        // pois a contagem do índice vai de 0 a n-1.
        return (this.getTopo()+1);
    }
    
    // Esvaziando a pilha
    public void clear(){

        // Se a pilha não estiver vazia, atualiza o valor de 'topo'.
        if (!this.isEmpty())
            this.setTopo(-1);
    }
    
    // Método para mostrar os valores da pilha
    public boolean print() throws IOException{
        
        if (!this.isEmpty()){

            // Percorre do topo até a posição 0.
            for (int i = this.getTopo(); i >= 0; i--){
                System.out.println("\nCódigo: " + this.getPilha()[i].getCodigoSala());
            }
            
            logTxt.gravarArquivo("Pilha de Ingressos Acessada");

            return true;
        }
        logTxt.gravarArquivo("Erro ao Acessar Fila Completa");
        return false;
    }
    
}
