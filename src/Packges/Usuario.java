/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packges;
/**
 * @author Kayc Kennedy
 */
public class Usuario {
    
    /*
              ATRIBUTOS DA CLASSE
    */
    
    private String nomeUsuario;
    private int numeroUsuario;
    private String codIngresso;
    private String emailUsuario;
  
    
    /*
             CONSTRUTOR DA CLASSE
     */    
    public Usuario(){ }
    
    public Usuario(String _nomeUsuario, int _numeroUsuario, String _emailUsuario, String _codIngresso ) {
        this.nomeUsuario   = _nomeUsuario;
        this.numeroUsuario = _numeroUsuario;
        this.emailUsuario = _emailUsuario;
        this.codIngresso = _codIngresso;
    }   
    
    /*
        MÉTODOS DA CLASSE
    */
 
    public String getNomeUsuario() {
        return this.nomeUsuario;
    }
    public void setNomeUsuario(String _nomeUsuario) {
        this.nomeUsuario = _nomeUsuario;
    }
    
    public void setNumeroUsuario (int _numeroU){
        this.numeroUsuario = _numeroU;
    }
    
    public int getNumeroUsuario (){
        return this.numeroUsuario;
    }
    
    public void setEmailUsuario (String _emailU){
        this.emailUsuario = _emailU;
    }
    
    public String getEmailUsuario (){
        return emailUsuario;
    }
    
    public void setCodIngresso (String _codIngresso){
        this.codIngresso = _codIngresso;
    }
    
    public String getCodIngresso (){
        return codIngresso;
    }
}