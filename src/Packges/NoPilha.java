/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Packges;

/**
 *
 * @author Kayc Kennedy
 */
public class NoPilha {
     /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private String codigoSala;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    
    // _codS  = código da sala
    public NoPilha(String _codS) {
        this.codigoSala = _codS;
    }
    
    /*-----------------------------------
                MÉTODOS DA CLASSE
    -----------------------------------*/
    
    // Método para resgatar o código do usuário
    public String getCodigoSala() {
        return this.codigoSala;
    }

    // Método para registrar o código do usuário
    public void setCodigoSala(String _codS) {
        this.codigoSala = _codS;
    }
}
